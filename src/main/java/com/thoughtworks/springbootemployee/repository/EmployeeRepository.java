package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    public List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lucy", 20, "female", 2000, 1L));
        employees.add(new Employee(2L, "Ken", 20, "male", 3000, 1L));
        employees.add(new Employee(3L, "Anson", 18, "male", 5000, 2L));
    }

    public List<Employee> findAllEmployee() {
        return employees;
    }

    public Employee findEmployeeByID(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Employee> findEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployeeById(Long id, Employee employee) {
        Employee updateEmployee = findEmployeeByID(id);
        updateEmployee.setAge(employee.getAge());
        updateEmployee.setSalary(employee.getSalary());
        return updateEmployee;
    }

    public Employee insertEmployee(Employee employee) {
        Long id = generateId();
        employee.setId(id);
        employees.add(employee);
        return employee;
    }

    private Long generateId() {
        return employees.stream()
                .mapToLong(employee -> employee.getId())
                .max()
                .orElse(0l) + 1;
    }

    public void deleteEmployeeById(Long id) {
        Employee deleteEmployee = findEmployeeByID(id);
        employees.remove(deleteEmployee);
    }

    public List<Employee> findEmployeeByPage(int pageNumber, int pageSize) {
        return employees.stream()
                .skip((pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Employee> findEmployeesByCompanyID(Long companyID) {
        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyID))
                .collect(Collectors.toList());
    }
}
