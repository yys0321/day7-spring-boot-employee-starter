package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Company;
import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyController(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.findAllCompanies();
    }

    @GetMapping(path = "/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyRepository.findCompanyByID(id);
    }

    @GetMapping(path = "/{id}/employees")
    public List<Employee> getEmployeesFromCompany(@PathVariable Long id) {
        return employeeRepository.findEmployeesByCompanyID(id);
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public List<Company> getCompanyByPage(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return companyRepository.findCompanyByPage(pageNumber, pageSize);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
        return companyRepository.insertCompany(company);
    }

    @PutMapping(path = "/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.updateCompanyById(id, company);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyRepository.deleteCompanyById(id);
    }
}
