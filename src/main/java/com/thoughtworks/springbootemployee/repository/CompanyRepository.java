package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    public List<Company> companies;

    public CompanyRepository() {
        this.companies = new ArrayList<>();
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "boot"));
    }

    public List<Company> findAllCompanies() {
        return companies;
    }

    public Company findCompanyByID(Long id) {
        return companies.stream()
                .filter(company -> company.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Company> findCompanyByPage(Integer pageNumber, Integer pageSize) {
        return companies.stream()
                .skip((pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company insertCompany(Company company) {
        Long id = generateId();
        company.setId(id);
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .mapToLong(company -> company.getId())
                .max()
                .orElse(0L) + 1;
    }

    public Company updateCompanyById(Long id, Company company) {
        Company updateCompany = findCompanyByID(id);
        updateCompany.setName(company.getName());
        return updateCompany;
    }

    public void deleteCompanyById(Long id) {
        Company deleteCompany = findCompanyByID(id);
        companies.remove(deleteCompany);
    }
}
